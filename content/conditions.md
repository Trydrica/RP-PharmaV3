+++
title = "Mes conditions de remplacement :"
description = "Mes conditions"
date = "01-05-2023"
aliases = []
author = "Romain POULIN"
+++



- Coefficient : 700
- Semaines minimum de 35h et journées consécutives
- Je ne souhaite pas bénéficier de la mutuelle d'entreprise. Il est nécessaire d'en parler à votre comptable et de lui transmettre le papier d'exemption de mutuelle que je vous fournirai.